# Tibbo recommended Setup
```bash
./setup.sh
cd poky
source oe-init-build-env build.tppg2
```

# local.conf changes
```bash
# jorge suggested
IMAGE_INSTALL:remove += "kernel-module-rtc-ds3232"

# Mender
MENDER_ARTIFACT_NAME = "release-1"
INHERIT += "mender-full"
DISTRO_FEATURES_append = " systemd"
VIRTUAL-RUNTIME_init_manager = "systemd"
DISTRO_FEATURES_BACKFILL_CONSIDERED = "sysvinit"
VIRTUAL-RUNTIME_initscripts = ""
ARTIFACTIMG_FSTYPE = "ext4"
MENDER_FEATURES_ENABLE_append = " mender-uboot"
MENDER_FEATURES_DISABLE_append = " mender-grub mender-image-uefi"

# build fixes
INITRAMFS_MAXSIZE = "434200"

```

# Mender layer (WIP)
```bash
git clone git@gitlab.com:esteban.aguero/meta-mender-tibbo.git poky/meta-mender-tibbo -b dunfell
git clone git@github.com:mendersoftware/meta-mender.git poky/meta-mender -b dunfell
bitbake-layers add-layer <full_path>/poky/meta-mender-tibbo
bitbake-layers add-layer <full_path>/poky/meta-mender
```

# Basic build
```bash
bitbake mc::img-tst-tini
```

# Create final binary
```bash
cd /disk2/build.tibbo.dunfell.0/tmp/deploy/images/tppg2
make -f sp_make.mk -d
```

# Flash binary to board
1. Format SD card to FAT32
2. Copy /disk2/build.tibbo.dunfell.0/tmp/deploy/images/tppg2/sp_out/ISPBOOOT.BIN to the root of the SD card
3. Jump CN10 & CN11 and insert SD card
4. Powerup the board, wait until complete the Flash until `ISP all: Done`
5. Unjump CN10 & CN11 and remove the SD card

# Useful resources
- https://docs.tibbo.com/phm/ltpp3g2_firmware
- https://github.com/tibbotech/yocto_layers