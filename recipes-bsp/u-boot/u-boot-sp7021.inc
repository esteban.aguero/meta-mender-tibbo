FILESEXTRAPATHS_prepend := "${THISDIR}/patches:"

# Disabling automatic patching
MENDER_UBOOT_AUTO_CONFIGURE = "0"

SRC_URI_append_mender-uboot = "${@bb.utils.contains('MENDER_UBOOT_AUTO_CONFIGURE', \
                                                        '1', \
                                                        '', \
                                                        ' file://0001-configs-sp7021-enable-mender-requirements.patch \
                                                          file://0001-Removed-root-references.patch \
                                                        ', \
                                                        d)}"

BOOTENV_SIZE ?= "0x20000"