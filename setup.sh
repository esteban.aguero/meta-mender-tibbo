#!/bin/bash

set -x

BRANCH="dunfell"
git clone -b ${BRANCH} git://git.yoctoproject.org/poky.git
git clone -b ${BRANCH} https://github.com/meta-qt5/meta-qt5.git ./poky/meta-qt5
git clone https://github.com/OpenAMP/meta-openamp.git ./poky/meta-openamp
git clone -b ${BRANCH} git://git.openembedded.org/meta-openembedded ./poky/meta-openembedded
git clone -b ${BRANCH} https://github.com/tibbotech/yocto_layers.git ./poky.x
rsync -a --exclude=.git ./poky.x/ ./poky/
patch -p0 < ./poky/npm.${BRANCH}.patch
sed -i 's/"honister/"dunfell honister/g' ./poky/meta-openamp/conf/layer.conf

cd ./poky; rm -rf ./meta-tibbo;  rm -rf ./meta-sunplus; cd ..
cd ./poky.x/;  git pull ; cd ..
rsync -a --exclude=.git ./poky.x/ ./poky/

cd ./poky;
git pull
cd ./meta-openembedded; git pull; cd ..
